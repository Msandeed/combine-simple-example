//
//  ViewController.swift
//  CombineFrameworkPractice
//
//  Created by Mostafa Sandeed on 2/18/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import UIKit
import Combine

class ViewController: UIViewController {
    
    @IBOutlet weak var allowMessagesSwitch: UISwitch!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var messageLabel: UILabel!
    
    @Published var canSendMessages:Bool = false     // Published keyword is a property wrapper and adds a Publisher to any property.
    private var switchSubscriber:AnyCancellable?    // AnyCancellable class calls cancel() on deinit and makes sure subscriptions terminate early. Without implementing this you can end up with retain cycles.
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubscriptions()
        
    }
    
    func setupSubscriptions() {
        
        /****WAY 1****/
        // Receive changes in published boolean and reflect on buttomn's isEnabled property
        switchSubscriber = $canSendMessages.receive(on: DispatchQueue.main).assign(to: \.isEnabled, on: sendButton)
        
        /****WAY 2****/
        // Create a publisher from a post notification. This publisher will listen for incoming notifications for the newMessage notification name. However, this will only happen as soon as there is a subscriber.
        let newMessagePublisher = NotificationCenter.Publisher(center: .default, name: .newMessage)
            .map { (notification) -> String? in
                return (notification.object as? Message)?.content ?? ""
            }
        // Subscribe to newMEssagePublisher and reflect incoming string to messageLabel's text property
        let newMesssageSubscriber = Subscribers.Assign(object: messageLabel, keyPath: \.text)
        newMessagePublisher.subscribe(newMesssageSubscriber)
    }

    @IBAction func didSwitch(_ sender: UISwitch) {
        canSendMessages = sender.isOn
    }
    
    
    @IBAction func sendMessage(_ sender: Any) {
        let message = Message(author: "Me", content: String(describing: Date()))
        NotificationCenter.default.post(name: .newMessage, object: message)
    }
}

extension Notification.Name {
    static let newMessage = Notification.Name("newMessage")
}

struct Message {
    let author: String?
    let content: String?
}

